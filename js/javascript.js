function mainpage() {
    var windowWidth = jQuery(window).width();
    var widthContainer = $('.container').width();
    var windowHeight = $(window).height();
    var heightHeader = $('header').height();
    var heightFooter = $('footer').height();
    $(".main-page").css("min-height", windowHeight - heightHeader - heightFooter);
}

$(document).ready(function () {
    $('header .navbar-toggle').click(function () {
        $('body').toggleClass('overflow');
    });

    $(window).scroll(function(){
        var sticky = $('.header'),
            scroll = $(window).scrollTop();

        if (scroll > 0) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });

    // $('.chart-item').click(function(){
    //     var t = $(this).attr('id');
    //
    //     if($(this).hasClass('active')){ //this is the start of our condition
    //         $('.chart-item').addClass('active');
    //         $(this).removeClass('active');
    //         $('.roadmap-content').hide();
    //         $('#'+ t + 'C').fadeIn('slow');
    //     }
    // });

    // $(".chart-item").click(function(){
    //     $(this).tab('show');
    // });

    // $('.datepicker').datepicker();

    $(window).scroll(function () {
        $(window).scrollTop() > 300 ? $(".go_top").addClass("go_tops") : $(".go_top").removeClass("go_tops")
    });

    var swiper5 = new Swiper(".slide-5 .swiper-container", {
        slidesPerView: 5,
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 1.5,
            },
            640: {
                slidesPerView: 3,
            },
            992: {
                slidesPerView: 4,
            },
            1200: {
                slidesPerView: 5,
            }
        },
        navigation: {
            nextEl: ".slide-5 .swiper-button-next",
            prevEl: ".slide-5 .swiper-button-prev",
        },
    });
    var swiper6 = new Swiper(".slide-6 .swiper-container", {
        slidesPerView: 3,
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            640: {
                slidesPerView: 2,
            },
            650: {
                slidesPerView: 3,
            },
        },
        navigation: {
            nextEl: ".slide-6 .swiper-button-next",
            prevEl: ".slide-6 .swiper-button-prev",
        },
    });
    var solution = new Swiper(".ebookValue .swiper-container", {
        slidesPerView: 4,
        breakpoints: {
            320: {
                slidesPerView: 1.2,
                spaceBetween: 10,
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            
        },
        navigation: {
            nextEl: ".ebookValue .swiper-button-next",
            prevEl: ".ebookValue .swiper-button-prev",
        },
    });

    var swiper4 = new Swiper(".slide-4  .swiper-container", {
        slidesPerView: 4,
        spaceBetween: 20,
        breakpoints: {
            320: {
                slidesPerView: 1.25,
            },
            640: {
                slidesPerView: 2,
            },
            992: {
                slidesPerView: 3,
            },
            1200: {
                slidesPerView: 4,
            }
        },
        navigation: {
            nextEl: ".slide-4 .swiper-button-next",
            prevEl: ".slide-4 .swiper-button-prev",
        },
    });

    var swiper1 = new Swiper(".slide-1 .swiper-container", {
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: ".slide-1 .swiper-button-next",
            prevEl: ".slide-1 .swiper-button-prev",
        },
    });

    mainpage();
    jQuery(window).resize(function () {
        mainpage();
    });

});


